const { ipcRenderer } = require('electron')
const { dialog } = require('electron').remote
require('tanana')

let libPath
ipcRenderer.send('read-file')
ipcRenderer.send('back-to-lib-window')

ipcRenderer.on('back-to-lib-window-reply', (event, arg) => (libPath = arg))

const renderSheet = (fileData) => {
  const scoreElem = document.querySelector('#main-score')
  const tanana = document.createElement('tanana-player')
  tanana.setAttribute('music-xml', fileData)
  // removing loading warning
  document.querySelectorAll('.loading').forEach(e => e.remove())
  scoreElem.appendChild(tanana)
}

const spacebarToggle = () => document.addEventListener('keyup', ev => {
  // space bar
  if (ev.keyCode === 32) {
    /* eslint-disable no-undef */
    if (tananaPlayerStore.state.playing) tananaPlayerStore.pauseAction()
    else tananaPlayerStore.playAction()
    /* eslint-enable no-undef */
  }
})
const preventSpacebarScroll = () => document.addEventListener('keydown', ev => {
  // space bar
  if (ev.keyCode === 32) {
    ev.preventDefault()
    return false
  }
})

ipcRenderer.on('read-file-reply', async (event, { fileData }) => {
  try {
    renderSheet(fileData)
    preventSpacebarScroll()
    spacebarToggle()
  } catch (err) {
    console.log(err)
    dialog.showErrorBox('Tananã - erro', err.message)
  }
  // TODO get title from osmd
  let title = false
  title = title ? title + ' | Tananã' : 'Tananã | Música Desconhecida'
  document.title = title
})

const goBack = () => {
  if (libPath) ipcRenderer.send('open-lib', libPath)
  else ipcRenderer.send('open-main-window')
}

document.querySelector('#back-button').addEventListener('click', goBack)

document.addEventListener('keyup', (ev) => {
  // escape key
  if (ev.keyCode === 27) goBack()
  else return false
})
