'use strict'

const gulp = require('gulp')
const sass = require('gulp-sass')
const electron = require('electron-connect').server.create()

const gulpSass = gulp.task('sass', function () {
  return gulp.src('./app/sass/**/style.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'))
})

gulp.task('watch', function () {
  // Start browser process
  electron.start()

  gulp.watch('./app/sass/**/*.scss', gulpSass)
  gulp.watch(['./app/**/*.js', './app/**/*.html'], electron.reload)
  gulp.watch('./app/main.js', electron.restart)
})
